import React from 'react';
import {StyleSheet, View} from 'react-native';
import {MeteoAdvanced} from '../../components/MeteoAdvanced';
import {MeteoBasic} from '../../components/MeteoBasic';
import {SearchBar} from '../../components/SearchBar';
import {getWeatherInterpretation} from '../../utils/meteo-utils';

export function Home({city, weather, onSubmitSearch}) {
  const currentWeather = weather.current_weather;
  const currentInterpretation = getWeatherInterpretation(
    currentWeather.weathercode,
  );
  return (
    <>
      <View style={styles.meteo_basic}>
        <MeteoBasic
          dailyWeather={weather.daily}
          city={city}
          interpretation={currentInterpretation}
          temperature={Math.round(currentWeather.temperature)}
        />
      </View>
      <View style={styles.searchbar_container}>
        <SearchBar onSubmit={onSubmitSearch} />
      </View>
      <View style={styles.meteo_advanced}>
        <MeteoAdvanced
          sunrise={weather.daily.sunrise[0].split('T')[1]}
          sunset={weather.daily.sunset[0].split('T')[1]}
          windspeed={currentWeather.windspeed}
        />
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  meteo_basic: {flex: 2},
  searchbar_container: {flex: 2},
  meteo_advanced: {flex: 1},
});
