import React, {useEffect, useState} from 'react';
import {StyleSheet} from 'react-native';
import {nowToHHMM} from '../utils/date-time';
import {Txt} from './Txt';

export function Clock() {
  const [time, setTime] = useState(nowToHHMM());

  useEffect(() => {
    // interval will keep running independently of component lifecycle
    const intervalId = setInterval(() => {
      setTime(nowToHHMM());
    }, 1000);

    // clean up function
    return () => {
      clearInterval(intervalId);
    };
  }, []);
  return (
    <>
      <Txt style={s.time}>{nowToHHMM()}</Txt>
    </>
  );
}

const s = StyleSheet.create({
  time: {
    fontSize: 15,
  },
});
