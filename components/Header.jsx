import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import {Txt} from './Txt';

export function Header({city}) {
  const nav = useNavigation();
  return (
    <View style={s.container}>
      <TouchableOpacity onPress={nav.goBack}>
        <Txt style={s.back_btn}>{'<'}</Txt>
      </TouchableOpacity>
      <View style={s.header_txts}>
        <Txt>{city}</Txt>
        <Txt style={s.subtitle}>7 day forecasts</Txt>
      </View>
    </View>
  );
}

const BACK_BTN_WIDTH = 30;
const s = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  back_btn: {
    width: BACK_BTN_WIDTH,
  },
  header_txts: {
    flex: 1,
    marginRight: BACK_BTN_WIDTH,
    alignItems: 'center',
  },
  subtitle: {
    fontSize: 20,
  },
});
