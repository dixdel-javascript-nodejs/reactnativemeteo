import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {Image, StyleSheet, TouchableOpacity, View} from 'react-native';
import {Clock} from './Clock';
import {Txt} from './Txt';

export function MeteoBasic({city, temperature, interpretation, dailyWeather}) {
  const nav = useNavigation();

  return (
    <>
      <View style={s.clock}>
        <Clock />
      </View>
      <View style={s.city}>
        <Txt>{city}</Txt>
      </View>
      <View style={s.interpretation}>
        <Txt style={s.interpretation_txt}>{interpretation.label}</Txt>
      </View>
      <View style={s.temperature_box}>
        <TouchableOpacity
          onPress={() => nav.navigate('Forecasts', {city, ...dailyWeather})}>
          <Txt style={s.temperature}>{temperature}°</Txt>
        </TouchableOpacity>
        <Image style={s.image} source={interpretation.image} />
      </View>
    </>
  );
}

const s = StyleSheet.create({
  clock: {
    alignItems: 'flex-end',
  },
  city: {},
  interpretation: {
    alignSelf: 'flex-end',
    transform: [{rotate: '-90deg'}],
  },
  interpretation_txt: {
    fontSize: 20,
  },
  temperature_box: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'baseline',
  },
  temperature: {fontSize: 150},
  image: {
    height: 90,
    width: 90,
  },
});
