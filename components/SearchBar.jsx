import React from 'react';
import {StyleSheet, TextInput} from 'react-native';

export function SearchBar({onSubmit}) {
  return (
    <TextInput
      onSubmitEditing={e => onSubmit(e.nativeEvent.text)}
      style={s.input}
      placeholder="Type a city... Ex: Shanghai"
      placeholderTextColor={'grey'}
    />
  );
}

const s = StyleSheet.create({
  input: {
    backgroundColor: 'white',
    height: 40,
    paddingLeft: 20,
    borderRadius: 20,
    fontFamily: 'Alata-Regular',
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    color: 'black',
  },
});
