import {useRoute} from '@react-navigation/native';
import React from 'react';
import {StyleSheet, View} from 'react-native';
import {ForecastListItem} from '../components/ForecastListItem';
import {Header} from '../components/Header';
import {DAYS, getWeatherInterpretation} from '../utils/meteo-utils';

export function Forecasts({}) {
  const {params} = useRoute();

  const forecastList = (
    <View style={s.list}>
      {params.time.map((time, index) => {
        const weathercode = params.weathercode[index];
        const image = getWeatherInterpretation(weathercode).image;
        const temperature = params.temperature_2m_max[index];
        const date = new Date(time);
        const dayOfTheWeek = DAYS[date.getDay()];
        const formatedDate = date.toLocaleDateString('default', {
          day: 'numeric',
          month: 'numeric',
        });
        return (
          <ForecastListItem
            key={time}
            image={image}
            day={dayOfTheWeek}
            date={formatedDate}
            temperature={temperature.toFixed(0)}
          />
        );
      })}
    </View>
  );
  return (
    <>
      <Header city={params.city} />
      {forecastList}
    </>
  );
}

const s = StyleSheet.create({
  list: {
    marginTop: 50,
  },
});
