import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React, {useEffect, useState} from 'react';
import {
  Alert,
  ImageBackground,
  PermissionsAndroid,
  Platform,
  StatusBar,
  StyleSheet,
} from 'react-native';
import Geolocation from 'react-native-geolocation-service';
import {PERMISSIONS, RESULTS, request} from 'react-native-permissions';
import {SafeAreaProvider, SafeAreaView} from 'react-native-safe-area-context';
import {MeteoAPI} from './api/meteo';
import backgroundImage from './assets/background.png';
import {Forecasts} from './pages/Forecasts';
import {Home} from './pages/Home/Home';
const Stack = createNativeStackNavigator();

const navTheme = {
  colors: {
    background: 'transparent',
  },
};

function App(): React.JSX.Element {
  const [coordinates, setCoordinates] = useState();
  const [weather, setWeather] = useState();
  const [city, setCity] = useState();

  useEffect(() => {
    getUserCoordinates();
  }, []);

  useEffect(() => {
    if (coordinates) {
      fetchWeatherByCoords(coordinates);
      fetchCityByCoords(coordinates);
    }
  }, [coordinates]);

  async function fetchWeatherByCoords(coords) {
    const weatherResponse = await MeteoAPI.fetchWeatherByCoords(coords);
    setWeather(weatherResponse);
  }

  async function fetchCityByCoords(coords) {
    const cityResponse = await MeteoAPI.fetchCityByCoords(coords);
    setCity(cityResponse);
  }

  async function fetchCoordsByCity(city) {
    try {
      const coordsResponse = await MeteoAPI.fetchCoordsByCity(city);
      setCoordinates(coordsResponse);
    } catch (err) {
      Alert.alert('Aouch !', err);
    }
  }

  async function requestLocationPermission() {
    if (Platform.OS === 'ios') {
      const status = await request(PERMISSIONS.IOS.LOCATION_WHEN_IN_USE);
      return status === RESULTS.GRANTED;
    } else {
      const status = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      );
      return status === PermissionsAndroid.RESULTS.GRANTED;
    }
  }

  async function getUserCoordinates() {
    const hasPermission = await requestLocationPermission();
    if (hasPermission) {
      Geolocation.getCurrentPosition(
        location => {
          setCoordinates({
            lat: location.coords.latitude,
            lng: location.coords.longitude,
          });
        },
        _ => {
          setDefaultCoordinates();
        },
        {enableHighAccuracy: true, timeout: 15000, maximumAge: 10000},
      );
    } else {
      setDefaultCoordinates();
    }
  }

  function setDefaultCoordinates() {
    setCoordinates({lat: '48.85', lng: '2.35'});
  }

  console.log(coordinates);
  console.log(weather);
  console.log(city);

  return (
    <NavigationContainer theme={navTheme}>
      <ImageBackground
        imageStyle={styles.image}
        source={backgroundImage}
        style={styles.image_background}>
        <StatusBar translucent backgroundColor="transparent" />
        <SafeAreaProvider>
          <SafeAreaView style={styles.container}>
            {weather && (
              <Stack.Navigator
                screenOptions={{headerShown: false, animation: 'fade'}}
                initialRouteName="Home">
                <Stack.Screen name="Home">
                  {() => (
                    <Home
                      city={city}
                      weather={weather}
                      onSubmitSearch={fetchCoordsByCity}
                    />
                  )}
                </Stack.Screen>
                <Stack.Screen name="Forecasts" component={Forecasts} />
              </Stack.Navigator>
            )}
          </SafeAreaView>
        </SafeAreaProvider>
      </ImageBackground>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  image_background: {
    flex: 1,
    width: '100%',
    height: '100%',
    backgroundColor: 'black',
  },
  image: {
    opacity: 0.75,
  },
});

export default App;
