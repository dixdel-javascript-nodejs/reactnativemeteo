import React from 'react';
import {StyleSheet, Text, useWindowDimensions} from 'react-native';
const IPHONE_13_RATIO = 0.001184834123222749;

export function Txt({children, style, ...otherProps}) {
  const fontSize = style?.fontSize || s.txt.fontSize;
  const {height} = useWindowDimensions();
  return (
    <Text
      style={[
        s.txt,
        style,
        {fontSize: Math.round(fontSize * IPHONE_13_RATIO * height)},
      ]}
      {...otherProps}>
      {children}
    </Text>
  );
}

const s = StyleSheet.create({
  txt: {
    fontSize: 30,
    color: 'white',
    fontFamily: 'Alata-Regular',
  },
});
