import React from 'react';
import {StyleSheet, View} from 'react-native';
import {Txt} from './Txt';

export function MeteoAdvanced({sunrise, sunset, windspeed}) {
  return (
    <View style={s.container}>
      <StyledContainer>
        <StyledLabel>{sunrise}</StyledLabel>
        <StyledValue>Sunrise</StyledValue>
      </StyledContainer>
      <StyledContainer>
        <StyledLabel>{sunset}</StyledLabel>
        <StyledValue>Sunset</StyledValue>
      </StyledContainer>
      <StyledContainer>
        <StyledLabel>{windspeed} km/h</StyledLabel>
        <StyledValue>Wind speed</StyledValue>
      </StyledContainer>
    </View>
  );
}
export function StyledContainer({children}) {
  return <View style={s.centered}>{children}</View>;
}

export function StyledLabel({children}) {
  return <Txt style={s.label}>{children}</Txt>;
}

export function StyledValue({children}) {
  return <Txt style={s.value}>{children}</Txt>;
}

const s = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: '#0000005c',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-evenly',
    borderColor: 'white',
    borderWidth: 2,
    borderRadius: 15,
  },
  centered: {
    alignItems: 'center',
  },
  label: {
    fontSize: 15,
  },
  value: {
    fontSize: 20,
  },
});
